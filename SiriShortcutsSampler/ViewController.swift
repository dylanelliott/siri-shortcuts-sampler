//
//  ViewController.swift
//  SiriShortcutsSampler
//
//  Created by Dylan Elliott on 28/6/18.
//  Copyright © 2018 Dylan Elliott. All rights reserved.
//

import UIKit
import Intents

class ViewController: UITableViewController {
    
    let items: [String] = [
        "toothpaste",
        "clay pot",
        "needle",
        "cell phone",
        "screw",
        "radio",
        "bottle cap",
        "outlet",
        "candle",
        "keyboard",
        "twister",
        "sailboat",
        "lotion",
        "purse",
        "grid paper",
        "newspaper",
        "bread",
        "rug",
        "car",
        "packing peanuts"
    ]
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell") ?? UITableViewCell(style: .default, reuseIdentifier: "Cell")
        
        cell.textLabel?.text = items[indexPath.row]
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let item = items[indexPath.row]
        donateIntent(withQuery: item)
    }
    
    func donateIntent(withQuery query: String) {
        let intent = SearchIntent()
        intent.query = query
        
        let interaction = INInteraction(intent: intent, response: nil)
        
        print("Donating intent...")
        
        interaction.donate { error in
            if let error = error {
                self.alert(title: "Error", message: "Could not donate intent\nReason: \(error.localizedDescription)")
            } else {
                self.alert(title: "Intent Donated", message: "\"Search for \(query)\" was donated succesfully")
            }
        }
    }
    
    func alert(title: String?, message: String?) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        present(alertController, animated: true, completion: nil)
    }
}

