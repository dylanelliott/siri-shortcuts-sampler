//
//  IntentHandler.swift
//  SiriShortcutsSampler
//
//  Created by Dylan Elliott on 28/6/18.
//  Copyright © 2018 Dylan Elliott. All rights reserved.
//

import Foundation
import Intents
import IntentsUI

class IntentHandler: INExtension, SearchIntentHandling {
    func handle(intent: SearchIntent, completion: @escaping (SearchIntentResponse) -> Void) {
        
        print("Handling intent!")
        
        guard let query = intent.query else {
            let response = SearchIntentResponse(code: .failure, userActivity: nil)
            completion(response)
            return
        }
        
        let response = SearchIntentResponse(code: .success, userActivity: nil)
        response.string = query
        response.number = NSNumber(integerLiteral: query.count)
        
        completion(response)
    }
    
}
