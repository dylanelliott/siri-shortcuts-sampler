//
//  IntentHandler.swift
//  SampleIntents
//
//  Created by Dylan Elliott on 29/6/18.
//  Copyright © 2018 Dylan Elliott. All rights reserved.
//

import Intents

// As an example, this class is set up to handle Message intents.
// You will want to replace this or add other intents as appropriate.
// The intents you wish to handle must be declared in the extension's Info.plist.

// You can test your example integration by saying things to Siri like:
// "Send a message using <myApp>"
// "<myApp> John saying hello"
// "Search for messages in <myApp>"

class IntentHandler: INExtension, SearchIntentHandling {
    func handle(intent: SearchIntent, completion: @escaping (SearchIntentResponse) -> Void) {
        
        var response = SearchIntentResponse(code: .failure, userActivity: nil)
        
        if let query = intent.query {
            response = SearchIntentResponse(code: .success, userActivity: nil)
            response.string = query
            response.number = query.count as NSNumber // Gets spoken by Siri as [object Object] instead of num value
        } else {
            response = SearchInte
        }
        
        completion(response)
    }
}
